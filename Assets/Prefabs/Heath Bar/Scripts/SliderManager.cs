using System;
using UnityEngine;

public class SliderManager : MonoBehaviour
{
    [SerializeField] SliderControllerAbstractBase[] _sliders;
    [Range(0f,1f)]
    [SerializeField] float percent = 1f;
    [SerializeField] private GameObject LifeHolder;
    private ILife _lifeHandler;
    [SerializeField]bool isPlayerLife;
    private void Awake()
    {
        if (isPlayerLife)
            LifeHolder = GameObject.FindGameObjectWithTag("Player");
        if (_sliders == null || _sliders.Length <= 0)
        {
            throw new Exception("Filler's Sprite Renderer is null.");
        }
        //_lifeHandler = GetComponentInParent<ILife>();
        _lifeHandler = LifeHolder.GetComponent<ILife>();
 
        if (_lifeHandler != null)
        {
            _lifeHandler.RegisterOnLifeChange(onLifeChanged);
        }

    }

    public void setLifeHolder()
    { 
    
    }
    public void onLifeChanged()
    {
        SetPercentage(_lifeHandler.GetHealtPercent());
        Debug.Log(name + "- LifeChange");
    }
    public void SetPercentage(float percent)
    {
        for (int i = 0; i < _sliders.Length; i++)
        {
            _sliders[i].SetPercentage(percent);
        }
        
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SetPercentage(percent);
        }
    }
}
