using UnityEngine;

public class LookAtMainCam : MonoBehaviour
{
    private void LateUpdate()
    {
        transform.LookAt((transform.position + Camera.main.transform.forward));
        transform.forward = -transform.forward;


        // transform.LookAt( Camera.main.transform);
    }
}
