using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISliderController : SliderControllerAbstractBase
{
    [SerializeField] Slider _slider;
    float _targetValue = 1f;

    private void Awake()
    {
        if (_slider == null)
        {
            throw new Exception("Filler Sprite Renderer is null.");
        }
    }

    public override void SetPercentage(float percent)
    {
        _targetValue = 1f * percent;
    }


    private void LateUpdate()
    {
        if (_slider.value == _targetValue)
            return;

        _slider.value = Mathf.MoveTowards(_slider.value, _targetValue, _speed * Time.deltaTime);
    }
}
