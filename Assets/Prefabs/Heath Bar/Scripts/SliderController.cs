using System;
using UnityEngine;

public class SliderController : SliderControllerAbstractBase
{
    [SerializeField] SpriteRenderer _fillerSpriteRenderer;
    Vector2 _defaultSize = Vector2.zero;
    Vector2 _targetSize;
    
    private void Awake()
    {
        if (_fillerSpriteRenderer == null)
        {
            throw new Exception("Filler Sprite Renderer is null.");
        }

        _defaultSize = _fillerSpriteRenderer.size;
        _targetSize = _defaultSize;
    }

    public override void SetPercentage(float percent)
    {
        _targetSize.x = _defaultSize.x * percent;
    }

    
    private void LateUpdate()
    {
        if (_fillerSpriteRenderer.size == _targetSize)
            return;

        _fillerSpriteRenderer.size = Vector2.MoveTowards(_fillerSpriteRenderer.size, _targetSize, _speed * Time.deltaTime);
    }

}
