using UnityEngine;

public abstract class SliderControllerAbstractBase : MonoBehaviour
{
    [SerializeField] protected float _speed = 1f;
    public abstract void SetPercentage(float percent);
    
}
