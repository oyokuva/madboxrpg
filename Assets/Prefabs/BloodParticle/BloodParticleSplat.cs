using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodParticleSplat : MonoBehaviour
{
    public ParticleSystem part;

    public List<ParticleCollisionEvent> collisionEvents;

   // public GameObject prefab;
    public float decaldistance;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        Collider col = other.GetComponent<Collider>();
        foreach (ParticleCollisionEvent particleCollisionEvent in collisionEvents)
        {
            
            if (col == null)
                return;

            if (particleCollisionEvent.normal.magnitude == 0)
                continue;

            GameObject splat = null;//EC.Managers.MainManager.Instance.PoolingManager.GetGameObjectById(PrefabPoolId);
            splat.transform.forward = -particleCollisionEvent.normal;
            splat.transform.position = other.GetComponent<Collider>().ClosestPoint( particleCollisionEvent.intersection) - splat.transform.forward * decaldistance; 
            splat.transform.localScale = Vector3.one * Random.Range(.4f, 2f);
            splat.transform.Rotate(0, 0, Random.Range(-180f, 180f));
            DecalDeleter deleter = splat.GetComponent<DecalDeleter>();
            if (deleter != null)
            {
             
                deleter.begin();
            }


        }
        Rigidbody rb = other.GetComponent<Rigidbody>();
        int i = 0;

        while (i < numCollisionEvents)
        {
            if (rb)
            {
                Vector3 pos = collisionEvents[i].intersection;
                Vector3 force = collisionEvents[i].velocity * 10;
                rb.AddForce(force);
            }
            i++;
        }
    }
}
