using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class DecalDeleter : MonoBehaviour
{
    public float life;
    public onLifeEnd LifeEnds;
  //  public DecalProjector UDP;
    void Start()
    {
        
    }
    public void begin()
    {
    //    UDP.fadeFactor = Random.Range(0.6f,1f); 
      //  StartCoroutine(LifeCycle());
    }


    IEnumerator LifeCycle()
    {
        /*while (UDP.fadeFactor >0)
        {
            yield return new WaitForEndOfFrame();
            UDP.fadeFactor = Mathf.MoveTowards(UDP.fadeFactor, 0, Time.deltaTime / life);
            
        }*/
        OnLifeEnd();
        return null;
    }
    public void OnLifeEnd()
    { 
    switch(LifeEnds)
        { 
            case onLifeEnd.Disable:
                gameObject.SetActive(false);
                break;
            case onLifeEnd.Destroy:
               Destroy(gameObject);
                break;
            case onLifeEnd.DestroyImmediate:
                DestroyImmediate(gameObject);
                break;


        }
    }
}


public enum onLifeEnd
{
    Destroy,
    DestroyImmediate,
    Disable

}