using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponChangeArgument : EventArgs
{
    public Weapon newWeapon;
}
