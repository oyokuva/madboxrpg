using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TargetRemoveArgument holding a ISimpleTarget
public class TargetRemoveArgument : ScriptableEventArgument
{
    public ISimpleTarget target;
}
