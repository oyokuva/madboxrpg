
// TargetSpawnedArgument holding a ISimpleTarget
public class TargetSpawnArgument : ScriptableEventArgument
{
    public ISimpleTarget target;
}
