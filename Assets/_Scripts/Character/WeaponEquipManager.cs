using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponEquipManager : MonoBehaviour,IInitializable
{
    
    [SerializeField] private ScriptableEvent _weaponChangedEvent;
    [SerializeField] private List<GameObject> StarterWeapons;
    [SerializeField] private GameObject _weaponSlot;
    private Weapon _equippedWeapon;
    private Weapon _thrownWeapon;
    private WeaponChangeArgument _weaponChangeArgument;
    public void EquipWeapon(GameObject weapon)
    {
        if(_weaponChangeArgument == null)
            _weaponChangeArgument = new WeaponChangeArgument();

        if (_equippedWeapon != null)
        {
            _thrownWeapon = _equippedWeapon;
            _thrownWeapon.VFXParticeSetEnabled(false);
            _equippedWeapon.SetPhysics(false);
            _thrownWeapon.transform.SetParent(null, false);
            _thrownWeapon.transform.position = transform.position;
            Rigidbody _thrownWeaponRigidBody = _thrownWeapon.GetComponent<Rigidbody>();

            _thrownWeaponRigidBody.isKinematic = false;
            _thrownWeaponRigidBody.velocity = Vector3.up * 10 - transform.forward * 2;
            _thrownWeaponRigidBody.angularVelocity = Vector3.up * 10 - _thrownWeapon.transform.right * 15;
            _thrownWeapon.transform.localScale = Vector3.one * 2;
            SoundManager.Play(SoundManager.Instance.OnWeaponChange);
            Invoke("flickPhysisc", 1);
     
        }
       
           
        _equippedWeapon = weapon.GetComponent<Weapon>();
        _weaponChangeArgument.newWeapon = _equippedWeapon;
        _equippedWeapon.SetPhysics(false);
        weapon.transform.SetParent(_weaponSlot.transform, false);
        weapon.transform.localRotation = Quaternion.identity;
        weapon.transform.localScale = Vector3.one;
        
        weapon.transform.localPosition = Vector3.zero;
        _weaponSlot.SetActive(false);

        AnounceChange();
    }

    public void flickPhysisc()
    {
        _thrownWeapon.SetPhysics(true);
    }


    public void AnounceChange()
    { 
    IWeaponEffected[] effectedComponents = GetComponents<IWeaponEffected>();
        foreach (IWeaponEffected effectedComponent in effectedComponents)
            effectedComponent.WeaponChanged(_equippedWeapon);
        _weaponChangedEvent.Invoke(_weaponChangeArgument);
    }
    public void Initialise()
    {
        EquipWeapon(Instantiate(StarterWeapons[Random.Range(0, StarterWeapons.Count)]));
    }
}
