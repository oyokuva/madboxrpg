using UnityEngine;

using System.Collections.Generic;
using System;

public class CharacterTargetSelector : MonoBehaviour,ITargetSelector<ISimpleTarget>,IInitializable,IScriptableEventListener,IWeaponEffected
{
    // CharacterTargetSelector class is used to get the closest target when needed and exists
    [SerializeField] private ScriptableEvent _onEnemySpawn;
    [SerializeField] private ScriptableEvent _onEnemyRemoved;   
    private ISimpleTarget _tempTarget;
    private DistanceComperator _distanceComperator;
    private Weapon _equipedWeapon;
    private List<ISimpleTarget> _targets;
    private float _autoSelectRange
    {
        get {
            if (_equipedWeapon == null)
                return GameManager.PARAMETERS.PlayerAutoSelectRange;
            else
                return _equipedWeapon.AttackRange;
        }
    } 

    public void AddTarget(ISimpleTarget target)
    {

        if(!_targets.Contains(target))
        _targets.Add(target);

    ///    Debug.Log("TargetAdded(" + _targets.Count + ")");
    }

    public ISimpleTarget GetNextTarget(Vector3 center)
    {
        if(_targets==null)
            return null;

        _tempTarget = null;
        if (_targets.Count != 0)
        {
            if (_distanceComperator == null)
                _distanceComperator = new DistanceComperator();

            _distanceComperator.CompareCenter = center;
            _targets.Sort(_distanceComperator);

            if( Vector3.Distance(_targets[0].GetGameObject().transform.position,center)  <_autoSelectRange)
                _tempTarget = _targets[0];

                
        }
       return _tempTarget;
    }

    public void Initialise()
    {
        _targets = new List<ISimpleTarget>();
        if (_onEnemySpawn != null)
            _onEnemySpawn.Register(this);
        if(_onEnemyRemoved != null)
            _onEnemyRemoved.Register(this);

    }

    public void RaiseEvent(EventArgs args)
    {
      //  Debug.Log("RaiseEvent:" + args.GetType());
        if (args.GetType().Equals(typeof(TargetSpawnArgument)))
        {
            AddTarget((args as TargetSpawnArgument).target);
        }
        else if (args.GetType().Equals(typeof(TargetRemoveArgument)))
        {
            RemoveTarget((args as TargetRemoveArgument).target);
        }

    }

    public void RemoveTarget(ISimpleTarget target)
    {
        _targets.Remove(target);
    }

    public void WeaponChanged(Weapon newWeapon)
    {
        _equipedWeapon = newWeapon;
    }
}

