using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour,ISimpleTarget,ILife,IInputBridge
{
    TargetRemoveArgument _targetRemoveArgument;
    [SerializeField]private  ScriptableEvent _EnemyDied;
   
    [SerializeField] private EnemyData _enemyData;
    [SerializeField] private FlyingText _flyingText;
    [SerializeField] private ParticleSystem _HitVFX;
    private ICharacterAnimationController _characterAnimationController;
    private event ILife.OnLifeChange _onLifeChange; 
    private float _maxLife;
    private float _currentLife;
    public void Die()
    {
        GameManager.PARAMETERS.PlayerPoints++;
        _enemyData.IncreaseLevel();
        SoundManager.Play(SoundManager.Instance.OnEnemyDieSound);

        if (_targetRemoveArgument == null)
            _targetRemoveArgument = new TargetRemoveArgument();

        _targetRemoveArgument.target = this;

        _EnemyDied.Invoke(_targetRemoveArgument);
        _characterAnimationController?.StartDeathAnimation();
    
        Invoke("LateDestroy", 1);
        
    }
    public void LateDestroy()
    {
        Destroy(gameObject);
    }
    public void GetDamage(float damage)
    {
        _flyingText.gameObject.SetActive(true);
        _flyingText.StartText(transform.position + Vector3.up, damage.ToString("F0"),Color.red);
        _currentLife -= damage;
        _HitVFX?.Play();
        if (IsDead())
           Die();

        _onLifeChange?.Invoke();
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public float GetHealt()
    {
       return _currentLife;
    }

    public float GetHealtPercent()
    {
        
        return _currentLife / _maxLife;
    }

    public float GetTotalDamageTaken()
    {
        return _maxLife - _currentLife;
    }

    public void Heal(float healt)
    {
        _currentLife += healt;
    }

    public bool IsDead()
    {
        return _currentLife <= 0;
    }

    public void RegisterOnLifeChange(ILife.OnLifeChange lifeChanged)
    {
        _onLifeChange += lifeChanged;
    }

    public void SetHealt(float healt)
    {
       _currentLife = healt;
    }

    public void Setup()
    {
        _currentLife = _enemyData.GetMaxHeath();
        _maxLife =  _enemyData.GetMaxHeath();
        GetComponent<EnemyAttackHandler>().setDamage(_enemyData.GetDamage());
        _characterAnimationController = GetComponentInChildren<ICharacterAnimationController>();
        (_characterAnimationController as IInitializable).Initialise(); 
    }

    public Vector2 GetInput()
    {
        return Vector2.zero;
    }
}
