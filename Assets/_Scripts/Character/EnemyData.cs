using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Data", menuName = "ScriptableObjects/Enemy")]
public class EnemyData : ScriptableObject,IResetable
{
    [SerializeField] private float _maxHealth;
    [SerializeField] private float _minHealth;
    [SerializeField] private float _maxDamage;
    [SerializeField] private float _minDamage;
    [SerializeField] private int _currentLevel;
    [SerializeField] private int _maxLevel;
    
    public int CurrentLevel { get => _currentLevel; }

    public float GetMaxHeath()
    {
        return Mathf.Lerp(_minHealth, _maxHealth, (float)CurrentLevel / _maxLevel);
    }
    public float GetDamage()
    {
        return Mathf.Lerp(_minDamage, _maxDamage, (float)CurrentLevel / _maxLevel);
    }

    public void IncreaseLevel()
    {
        _currentLevel = Mathf.Clamp(CurrentLevel + 1, 0, _maxLevel);
    }

    public void ResetLevel()
    {
        _currentLevel = 0;
    }
    private void OnDisable()
    {
       
    }

    public void Reset()
    {
        ResetLevel();
    }
}