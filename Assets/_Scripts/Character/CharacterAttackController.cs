using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IInputBridge))]
public class CharacterAttackController : Input2DUser,IWeaponEffected
{
    private bool _isMoving;
    private bool _isAttacking;

    private IInputBridge _characterInput;
    private ICharacterAnimationController _characterAnimationController;
    private Weapon _equippedWeapon;
    [SerializeField] CharacterTargetSelector _targetSelector;
    [SerializeField] GameObject WeaponSlot;
    private ISimpleTarget CurrentTarget;



    public void AttackLanded()
    {
        if (CurrentTarget != null)
        {

            SoundManager.Play(SoundManager.Instance.OnPlayerHitSound);
            SoundManager.Play(SoundManager.Instance.OnSwordLandSound);
            CurrentTarget.GetGameObject()?.GetComponent<ILife>()?.GetDamage(GameManager.PARAMETERS.PlayerDefaultDamage);
          Invoke("EndAttack" ,.2f);
        }


    }
    public void AttackAnimationEventAction(int EventId)
    { 
        switch (EventId)
        { 
            case 0:
                AttackLanded();
                break;


            default:
                break;
        }
    }
    public override void Initialise()
    {
        base.Initialise();
        _characterAnimationController = GetComponentInChildren<ICharacterAnimationController>();
        _characterAnimationController.RegisterAttackEvent(AttackAnimationEventAction);
    }

    private void LateUpdate()
    {

        // _isMovingCheck
        if (_getInput() != Vector3.zero)
        {
            if (!_isMoving)
            {
                _isMoving = true;
                EndAttack();
            }
        }
        else
        {
            if (_isMoving)
            {
                _isMoving = false;
            }
        }

        if (!_isAttacking&&!_isMoving)
        {
            if(CurrentTarget==null)
                CurrentTarget = _targetSelector.GetNextTarget(transform.position);

            if (CurrentTarget != null)
            {

                StartAttack();
            }


        }
        

    }
    public void StartAttack()
    {
        if (_characterAnimationController == null)
            return;

        WeaponSlot.SetActive(true);
        Debug.Log("StartAttack");
        _characterAnimationController.StartAttackAnimation();
        transform.LookAt(CurrentTarget.GetGameObject().transform); // Daha yavaşşş
        _isAttacking = true;
    }
    public void EndAttack()
    {
        WeaponSlot.SetActive(false);
        CurrentTarget = null;
        _characterAnimationController.EndAttackAnimation();
        _isAttacking = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if(CurrentTarget!=null)
        Gizmos.DrawSphere(CurrentTarget.GetGameObject().transform.position, .5f);

    }

    private void weaponChangeDelayed()
    {
        WeaponChanged(_equippedWeapon);
    }
    public void WeaponChanged(Weapon newWeapon)
    {
         _equippedWeapon = newWeapon;

        if (_characterAnimationController == null)
        {
            Invoke("weaponChangeDelayed", .1f);
            return;
        }
        if (_equippedWeapon!=null)
            _characterAnimationController.SetAttackSpeed(_equippedWeapon.AnimationSpeedMultiplier);
        else
            _characterAnimationController.SetAttackSpeed(1);
    }
}
