using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour,ILife
{
    [SerializeField] private ScriptableEvent OnPlayerDeathEvent;
    [SerializeField] private CharacterAnimationController _characterAnimationController;
    [SerializeField] private CharacterAttackController _characterAttackController;
    [SerializeField] private CharacterMovementController _characterMovementController;
    [SerializeField] private CharacterTargetSelector _characterTargetSelector;
    [SerializeField] private WeaponEquipManager _weaponEquipManager;

    private event ILife.OnLifeChange lifeChanged;
    private float _life = 300;
    private float _lifeMax = 300;

    public void Die()
    {
        _characterAnimationController.StartDeathAnimation();
        OnPlayerDeathEvent.Invoke(new PlayerDeadArgument());
        SoundManager.Play(SoundManager.Instance.OnPlayerDie);
    }

    public void GetDamage(float damage)
    {
        if (IsDead())
            return;

        _life -= damage;
        lifeChanged?.Invoke();
        if(IsDead())
            Die();
        else
            SoundManager.Play(SoundManager.Instance.OnPlayerDamageSound);

    }

    public float GetHealt()
    {
       return _life;
    }

    public float GetHealtPercent()
    {
       return _life/ _lifeMax;
    }

    public float GetTotalDamageTaken()
    {
        return 0;
    }

    public void Heal(float healt)
    {
        _life += healt;
    }

    public bool IsDead()
    {
        return _life <= 0;
    }

    public void RegisterOnLifeChange(ILife.OnLifeChange lifeChanged)
    {
        this.lifeChanged += lifeChanged;
      
    }

    public void SetHealt(float healt)
    {
       _life = healt;
    }

    public void Setup()
    {
        _life = _lifeMax;
    }

    IEnumerator Start()
    {
      
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
        while (!SceneManager.GetSceneByBuildIndex(1).isLoaded)
            yield return new WaitForSeconds(.5f);
        _weaponEquipManager.Initialise();
        _characterAnimationController.Initialise();
        _characterMovementController.Initialise();
        _characterTargetSelector.Initialise();
        _characterAttackController.Initialise();
        

    }
    
}
