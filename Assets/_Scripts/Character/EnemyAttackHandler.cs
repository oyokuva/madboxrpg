using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackHandler : MonoBehaviour
{
    private GameObject _playerObject;
    [SerializeField] private float _damage = 30;
    private ICharacterAnimationController _characterAnimationController;
    public float _attackDistance = 3;
    private bool _isAttacking;
    public void setDamage(float damage)
    {
        _damage = damage;
    }
   
    // Update is called once per frame
    void Update()
    {


        if (_playerObject == null)
            _playerObject = GameObject.FindGameObjectWithTag("Player");

        if (_characterAnimationController == null)
        {
            _characterAnimationController = GetComponentInChildren<ICharacterAnimationController>();
            _characterAnimationController.RegisterAttackEvent(AttackLanded);
        }

        if (Vector3.Distance(transform.position, _playerObject.transform.position) < _attackDistance)
        {
            if (!_isAttacking)
            {
                StartAttack();

            }
            transform.LookAt(_playerObject.transform.position);
        }
        else
        {
            
            if (_isAttacking)
            {
            StopAttack();

            }
        }


    }
    public void AttackLanded(int eventId)
    {
        if (Vector3.Distance(transform.position, _playerObject.transform.position) < _attackDistance)
            _playerObject.GetComponent<ILife>().GetDamage(_damage);
        
    }
    public void StartAttack()
    {
     _characterAnimationController.StartAttackAnimation();
    _isAttacking = true;
    }

    public void StopAttack()
    {
        _characterAnimationController.EndAttackAnimation();
        _isAttacking &= false;
    }

}
