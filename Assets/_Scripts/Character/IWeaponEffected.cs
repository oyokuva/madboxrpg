using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponEffected 
{
    public void WeaponChanged(Weapon newWeapon);
}
