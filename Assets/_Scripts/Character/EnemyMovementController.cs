using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovementController : MonoBehaviour
{
    private GameObject _playerObject;
    private ICharacterAnimationController _characterAnimationController;
    public float _attackDistance=1;
    private bool _isMoving;
    [SerializeField]private NavMeshAgent _agent;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerObject == null)
            _playerObject = GameObject.FindGameObjectWithTag("Player");

        if(_characterAnimationController==null)
            _characterAnimationController = GetComponentInChildren<ICharacterAnimationController>();

        if (Vector3.Distance(transform.position, _playerObject.transform.position) < _attackDistance)
        {
            if (_isMoving)
            {
                Stop();

            }
        }
        else
        {
            _agent.SetDestination(Vector3.MoveTowards(_playerObject.transform.position, transform.position, _attackDistance));
            if (!_isMoving)
            {
                Move();

            }
        }

    }
    public void Stop()
    {
        _isMoving = false;
        _agent.speed = 0;
        _characterAnimationController.StartRunAnimation();
    }
    public void Move()
    {


        _isMoving=true;
        _agent.speed = 1;
      
        _characterAnimationController.StartIdleAnimation();
    }
}

