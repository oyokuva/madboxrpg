using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class CharacterMovementController : Input2DUser, IWeaponEffected
{
    // CharacterMovementController connects to imput bridge adapten on the character
    // Moves the character due to input vector using Navmesh. 
    
    private Weapon _equippedWeapon;  
    private NavMeshAgent _navMeshAgent;
    private ICharacterAnimationController _characterAnimationController;
    private bool _isMoving;
    private float _CharacterMoveSpeed
    {
        get
        {
            if (_equippedWeapon != null)
                return GameManager.PARAMETERS.PlayerDefaultSpeed * _equippedWeapon.MovementSpeedModifier;
            else
                return GameManager.PARAMETERS.PlayerDefaultSpeed;

        }
    }

    public override void Initialise()
    {
       base.Initialise();
      
      _navMeshAgent = GetComponent<NavMeshAgent>();
      _characterAnimationController = GetComponentInChildren<ICharacterAnimationController>();
      _navMeshAgent.updateRotation = true;
    }



    private void LateUpdate()
    {
        if (_getInput() != Vector3.zero)
        {
            _navMeshAgent.Move(Time.deltaTime *-1* _getInput().normalized * _CharacterMoveSpeed);
            transform.forward = Vector3.MoveTowards(transform.forward, -1*_getInput(), 
                Time.deltaTime * GameManager.PARAMETERS.PlayerRotateSpeed);
            if (!_isMoving)
            {
                _characterAnimationController.StartRunAnimation();
                _isMoving = true;
            }

        }
        else
        {
            if (_isMoving)
            {
                _characterAnimationController.StartIdleAnimation();
                _isMoving = false;
            }
        }

    }

    private void weaponChangeDelayed()
    {
        WeaponChanged(_equippedWeapon);
    }
    public void WeaponChanged(Weapon newWeapon)
    {
      //  Debug.Log("WeaponChangeArrive");
        _equippedWeapon=newWeapon;
        if (_characterAnimationController == null)
        {
            Invoke("weaponChangeDelayed", .1f);
            return;
        }
        if (_equippedWeapon != null)
            _characterAnimationController.SetRunSpeed(_equippedWeapon.MovementSpeedModifier);
        else
            _characterAnimationController.SetRunSpeed(1);

    }
}
