using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimationEventListener<T> 
{
    public delegate void AttackAnimationEventAction(T EventId);
    public void RegisterAttackEvent(AttackAnimationEventAction eventAction);
 

}
