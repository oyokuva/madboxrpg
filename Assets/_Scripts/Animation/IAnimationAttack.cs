using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimationAttack
{
    public void StartAttackAnimation();
    public void EndAttackAnimation();
    public void AttackEventCall(int EventId);
    public void SetAttackSpeed(float attackSpeed);
}
