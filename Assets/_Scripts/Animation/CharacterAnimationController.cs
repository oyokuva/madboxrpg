using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//Tested

public class CharacterAnimationController : MonoBehaviour,ICharacterAnimationController,IInitializable
{

    // CharacterAnimationController is the basic implementation of ICharacterAnimationController for our character animations
    // will be identified and managed by other classes via ICharacterAnimationController; 
    [SerializeField]private Animator _animator;
    [SerializeField] private string _attackAnimationName;
    [SerializeField] private string _takeDamageAnimationName;
    [SerializeField] private string _dieAnimationName;
    [SerializeField] private string _attackSpeedParameterName;
    [SerializeField] private string _runSpeedParameterName;
    [SerializeField] private string _isRunningParameterName;


    private event ICharacterAnimationController.AttackAnimationEventAction AnimationEventCall;
    private int _attackAnimationHash;
    private int _takeDamageAnimationHash;
    private int _isRunningParameterHash;
    private int _dieAnimationHash;
    private int _attackSpeedParameterHash;
    private int _runSpeedParameterHash;

    public void Initialise()
    {
        // We get the hash codes so we can get faster reactions from animator;
        _attackAnimationHash = Animator.StringToHash(_attackAnimationName);
        _takeDamageAnimationHash = Animator.StringToHash(_takeDamageAnimationName);
        _attackSpeedParameterHash = Animator.StringToHash(_attackSpeedParameterName);
        _dieAnimationHash = Animator.StringToHash(_dieAnimationName);
        _runSpeedParameterHash = Animator.StringToHash(_runSpeedParameterName);
        _isRunningParameterHash = Animator.StringToHash(_isRunningParameterName);
    }

    public void RegisterAttackEvent(ICharacterAnimationController.AttackAnimationEventAction eventAction)
    {
        AnimationEventCall += eventAction;
    }

    
    public void SetAttackSpeed(float attackSpeed)
    {
        _animator.SetFloat(_attackSpeedParameterHash, attackSpeed);
    }
    public void SetRunSpeed(float runSpeed)
    {

        _animator.SetFloat(_runSpeedParameterHash, runSpeed);
    }

    public void StartAttackAnimation()
    {

        _animator.SetBool("isAttacking",true);
    }

    public void StartDeathAnimation()
    {
        _animator.Play(_dieAnimationHash);
    }

    public void StartIdleAnimation()
    {
        _animator.SetBool(_isRunningParameterHash, false);
       
    }

    public void StartRunAnimation()
    {
        _animator.SetBool(_isRunningParameterHash, true);
    }

    public void AttackEventCall(int EventId)
    {
        AnimationEventCall?.Invoke(EventId);
    }

    public void StartTakeDamageAnimation()
    {
     
        _animator.Play(_takeDamageAnimationHash);
    }

    public void EndAttackAnimation()
    {
        _animator.SetBool("isAttacking", false);
    }
}
