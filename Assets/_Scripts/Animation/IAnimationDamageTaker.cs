using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimationDamageTaker 
{
    public void StartDeathAnimation();
    public void StartTakeDamageAnimation();
}
