using System;

//ICharacterAnimationController is created to define animation methods and 
//return the animationEventCalls from the Animation controller. 
//It can be refactored with a more basic base IBasicAnimationController and can be moved
//to an abstract class for parameter sets and animation calls. 

public interface ICharacterAnimationController :IAnimationEventListener<int>,IAnimationAttack, IAnimationMovement
    , IAnimationDamageTaker{}
