using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimationMovement 
{
    public void StartRunAnimation();
    public void StartIdleAnimation();
    public void SetRunSpeed(float runSpeed);
}
