using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISwordImageChanger : MonoBehaviour,IScriptableEventListener
{
   [SerializeField] private ScriptableEvent WeaponChanged;
    [SerializeField] private Image UIImage;

    private void Awake()
    {
        WeaponChanged.Register(this);
    }

    public void RaiseEvent(EventArgs args)
    {
        if (args.GetType().Equals(typeof(WeaponChangeArgument)))
        {
            UIImage.sprite = (args as WeaponChangeArgument).newWeapon.UISprite;
        }
    }
    private void OnDestroy()
    {
        WeaponChanged.Unregister(this);
    }



}
