
using UnityEngine;

public interface IGameObjectAccessor 
{
    public GameObject GetGameObject();
}
