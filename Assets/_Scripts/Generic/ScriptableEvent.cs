using System;

using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameEvent", menuName = "Events/ScriptableEvent")]
public class ScriptableEvent : ScriptableObject
{
        HashSet<IScriptableEventListener> _Listeners = new HashSet<IScriptableEventListener>();
        public void Invoke(EventArgs args = null)
        {
          //  Debug.Log("EventInvoke:"+name);
             
            foreach (IScriptableEventListener listener in _Listeners)
                listener.RaiseEvent(args);
        }
        public void JustInvoke() => Invoke();

        public void Register(IScriptableEventListener listener) => _Listeners.Add(listener);
        public void Unregister(IScriptableEventListener listener) => _Listeners.Remove(listener);
}
