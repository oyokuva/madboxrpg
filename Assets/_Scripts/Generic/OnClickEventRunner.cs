using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickEventRunner : MonoBehaviour
{
   [SerializeField] private UnityEngine.Events.UnityEvent OnClick;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnClick?.Invoke();
        }
    }
   
}
