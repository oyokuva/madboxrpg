using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITargetSelector<T> 
{
    public void AddTarget(T target);
    public void RemoveTarget(T target);
    public T GetNextTarget(Vector3 center);
}
