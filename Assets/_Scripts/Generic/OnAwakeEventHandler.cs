using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAwakeEventHandler : MonoBehaviour
{
    [SerializeField] private UnityEngine.Events.UnityEvent OnAwake;
    private void Awake()
    {
            OnAwake?.Invoke();
    }
}
