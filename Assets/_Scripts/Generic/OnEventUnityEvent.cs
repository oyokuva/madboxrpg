using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEventUnityEvent : MonoBehaviour,IScriptableEventListener
{
    [SerializeField] private ScriptableEvent CallEvent;
    public UnityEvent CallList;

    private void Awake()
    {
     CallEvent.Register(this);   
    }

    public void RaiseEvent(EventArgs args)
    {
        CallList?.Invoke();
    }
}
