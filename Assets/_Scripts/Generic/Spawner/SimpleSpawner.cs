using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSpawner : MonoBehaviour,IScriptableEventListener
{

    
    [SerializeField] private ScriptableEvent EnemyAdded;
    [SerializeField] private ScriptableEvent EnemyAddedRemoved;
    [SerializeField] private ScriptableEvent PlayerDead;
    [SerializeField] private ScriptableEvent GameStarted;
    [SerializeField] private GameObject SpawnCenterReference;
    [SerializeField] private GameObject EnemyPrefab;
    [SerializeField] private float spawnDelay = 10;
    [SerializeField] private int MaxCrowd  = 10;
    private List<GameObject> spawnList = new List<GameObject>();




    private void Awake()
    {
        GameStarted.Register(this);
        EnemyAddedRemoved.Register(this);
        PlayerDead.Register(this);  
    }
    public void spawn()
    {

        if (spawnList.Count < MaxCrowd)
        {
            GameObject enemy = Instantiate(EnemyPrefab, randomSpawnLocation(), Quaternion.identity);
            spawnList.Add(enemy);
            TargetSpawnArgument targetSpawnedArgument = new TargetSpawnArgument();
            targetSpawnedArgument.target = enemy.GetComponent<EnemyController>();
            (targetSpawnedArgument.target as EnemyController)?.Setup();
            EnemyAdded.Invoke(targetSpawnedArgument);
        }
        Invoke("spawn", spawnDelay);

    }

    public Vector3 randomSpawnLocation()
    {

        return new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10));

    }

    public void RaiseEvent(System.EventArgs args)
    {
        if (args == null)
            spawn();
        else if (args.GetType().Equals(typeof(TargetRemoveArgument)))
        {
            spawnList.Remove((args as TargetRemoveArgument).target.GetGameObject());
        }
        else if (args.GetType().Equals(typeof(PlayerDeadArgument)))
        {
           Destroy(gameObject);
        }

    }
    private void OnDestroy()
    {
        GameStarted.Unregister(this);
        EnemyAddedRemoved.Unregister(this);
        PlayerDead.Unregister(this);
    }
}
