using UnityEngine;
public interface IInputBridge
{
    Vector2 GetInput();
}