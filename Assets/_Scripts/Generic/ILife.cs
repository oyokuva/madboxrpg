using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ILife
{   delegate void OnLifeChange();

    void RegisterOnLifeChange(OnLifeChange lifeChanged);
    void Setup();
    void GetDamage(float damage);
    float GetTotalDamageTaken();
    float GetHealt();
    float GetHealtPercent();
    void SetHealt(float healt);
    void Heal(float healt);
    bool IsDead();
    void Die();
}
