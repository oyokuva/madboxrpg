using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTestKeyboard : MonoBehaviour,IInputBridge
{

    // This is a direction input test class for Input Bridge.

    Vector2 PlayerInput;
    public Vector2 GetInput()
    {
        return PlayerInput;
    }
    void Update()
    {

        if (PlayerInput == null)
            PlayerInput = new Vector2();

        if (Input.GetKey(KeyCode.LeftArrow))
            PlayerInput.x = +1;
        else if (Input.GetKey(KeyCode.RightArrow))
            PlayerInput.x = -1;
        else
            PlayerInput.x = 0;

        if (Input.GetKey(KeyCode.DownArrow))
            PlayerInput.y = +1;
        else if (Input.GetKey(KeyCode.UpArrow))
            PlayerInput.y = -1;
        else
            PlayerInput.y = 0;

    }

}
