using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Parameters", menuName = "ScriptableObjects/Game Parameters")]
public class GameParameters : ScriptableObject
{
    [Header("Default Values")]
    [SerializeField] float _animationSpeed = 1f;
    [SerializeField] float _playerDefaultDamage = 40f;
    [SerializeField] float _playerDamageRandomFactor = .2f;
    [SerializeField] float _movementSpeed = 1f;
    [SerializeField] float _attackRange = 1f;
    [SerializeField] float _playerRotateSpeed = 30f;
    [SerializeField] float _playerDefaultSpeed = 3f;
    [SerializeField] float _playerDefaultRange = 2f;
    
    public bool loaded = false;
    [SerializeField]private int _playerPoints;
    public float AnimationSpeed { get => _animationSpeed; }
    public float MovementSpeed { get => _movementSpeed; }
    public float AttackRange { get => _attackRange; }
    public float PlayerDefaultDamage { get => _playerDefaultDamage* Random.Range(1f- _playerDamageRandomFactor,1f+ _playerDamageRandomFactor) ; }

    public float PlayerDefaultSpeed { get => _playerDefaultSpeed; }
    public float PlayerRotateSpeed { get => _playerRotateSpeed; }
    public float PlayerAutoSelectRange { get => _playerDefaultRange; }

    public int PlayerPoints
    {
        get
        {
            if (!loaded)
            {
                loaded = true;
                _playerPoints = PlayerPrefs.GetInt("PlayerPoints", 0);
            }
            return _playerPoints;

        }
        set {
            _playerPoints = value;
            PlayerPrefs.SetInt("PlayerPoints", _playerPoints);
        }
    }

    private void OnDisable()
    {
        loaded = false; 
    }

}
