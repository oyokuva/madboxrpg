public interface IInitializable 
{
    // This interface is used to call Initialize required objects initialization method 
    // it is necessary for the objects that require initialization to be made 
    // independent from default monobehaviour events such as start or awake
   public void Initialise();
}
