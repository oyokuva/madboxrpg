using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    [Header("Settings")]
    public bool IsSoundActive = true;
    public readonly string saveId = "_soundStatus";

    [SerializeField] AudioSource audioSource;
    [Space]

    [Header("Sound Clips")]
    public AudioClip OnPlayerHitSound;
    public AudioClip OnEnemyHitSound;
    public AudioClip OnSwordLandSound;
    public AudioClip OnEnemyDieSound;
    public AudioClip OnPlayerDamageSound;
    public AudioClip OnPlayerDie;
    public AudioClip OnWeaponChange;


    public static void Play(AudioClip clip)
    {
        if (Instance.IsSoundActive && clip != null)
            Instance.audioSource.PlayOneShot(clip);
    }

    public static void SoundSwicth()
    {
        Instance.IsSoundActive = !Instance.IsSoundActive;
        Save();
    }

    private static void Load()
    {
        Instance.IsSoundActive = PlayerPrefs.GetInt(Instance.saveId, 1) == 1;
    }

    private static void Save()
    {
        PlayerPrefs.SetInt(Instance.saveId, Instance.IsSoundActive ? 1 : 0);
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        if (!TryGetComponent(out audioSource))
            Debug.LogWarning("There is no Audios Source on " + gameObject.name);

        Load();
    }



}