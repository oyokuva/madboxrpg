using System;

public interface IScriptableEventListener 
{
    public void RaiseEvent(EventArgs args);
}

