
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemSpawner : MonoBehaviour,IScriptableEventListener
{
    [SerializeField] ScriptableEvent OnEnemyDeath;
    [SerializeField] ScriptableEvent OnPlayerDeath;
    [SerializeField] private List<GameObject> StarterWeapons;
    GameObject spawnedWeapon;
    Weapon spawnedWeaponClass;

    private void Awake()
    {
        if (OnEnemyDeath != null)
            OnEnemyDeath.Register(this);
        if (OnPlayerDeath != null)
            OnPlayerDeath.Register(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void spawn(Vector3 position)
    { 
    int index = Random.Range(0, StarterWeapons.Count+1);

        if (index >= StarterWeapons.Count)
            return;
        else {
            GameObject spawnedWeapon = Instantiate(StarterWeapons[index]);
            spawnedWeapon.transform.position = position + Vector3.up;
            spawnedWeaponClass = spawnedWeapon.GetComponent<Weapon>();
            spawnedWeaponClass.SetPhysics(false);
            spawnedWeaponClass.VFXParticeSetEnabled(false);
            spawnedWeapon.GetComponent<Rigidbody>().isKinematic = false;
            spawnedWeapon.GetComponent<Rigidbody>().velocity = Vector3.up * 10 - spawnedWeapon.transform.forward*2;
            spawnedWeapon.GetComponent<Rigidbody>().angularVelocity = Vector3.up * 10 - spawnedWeapon.transform.right*15*Random.Range(1f,2f)  +
                 spawnedWeapon.transform.forward * 15 * Random.Range(1f, 2f);
            spawnedWeapon.transform.localScale = Vector3.one * 2;
            Invoke("flickPhysisc", 1);
        } 
            
    }
    public void flickPhysisc()
    { 
    
        spawnedWeaponClass.SetPhysics(true);
    }
    public void RaiseEvent(EventArgs args)
    {
        if (args.GetType().Equals(typeof(TargetRemoveArgument)))
        {
            spawn((args as TargetRemoveArgument).target.GetGameObject().transform.position);
        }
        if (args.GetType().Equals(typeof(PlayerDeadArgument)))
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
            OnEnemyDeath.Unregister(this);
            OnPlayerDeath.Unregister(this);
    }
}
