using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static GameParameters PARAMETERS => instance.gameParameters;
    public GameParameters gameParameters;
    private void Awake()
    {
        if (instance != null)
        { 
            Destroy(gameObject);
            return;
        }
        
        instance = this;


    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public class DistanceComperator : IComparer<ISimpleTarget>
{
    public Vector3 CompareCenter;
    public int Compare(ISimpleTarget x, ISimpleTarget y)
    {
        return Vector3.Distance(x.GetGameObject().transform.position, CompareCenter) >
            Vector3.Distance(y.GetGameObject().transform.position, CompareCenter) ? 1 : -1;
    }

}
