using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResetable 
{
    // Start is called before the first frame update
    public void Reset();
}
