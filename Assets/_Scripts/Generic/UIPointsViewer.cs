using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIPointsViewer : MonoBehaviour,IScriptableEventListener
{
    [SerializeField] private GameParameters _gameParameters;
    [SerializeField] private TextMeshProUGUI _textMeshProUGUI;
    [SerializeField] private ScriptableEvent OnEnemyDeath;

    public void RaiseEvent(EventArgs args)
    {
        _textMeshProUGUI.text = "" + _gameParameters.PlayerPoints;
    }


    // Start is called before the first frame update
    void Start()
    {
        OnEnemyDeath.Register(this);
        _textMeshProUGUI.text = ""+_gameParameters.PlayerPoints;
    }

    // Update is called once per frame
    
}
