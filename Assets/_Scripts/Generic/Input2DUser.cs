using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(IInputBridge))]
public class Input2DUser : MonoBehaviour, IInitializable
{
    private Vector3 _input;
    private IInputBridge _inputBridge;

    public virtual void Initialise()
    {
        _inputBridge = GameObject.FindGameObjectWithTag("InputBridge").GetComponent<IInputBridge>();
      //  Debug.Log(_inputBridge.ToString());
    }

    protected Vector3 _getInput()
    {
        if (_inputBridge == null)
            return Vector3.zero;
        if (_input == null)
            _input = new Vector3();

        
        _input.x = _inputBridge.GetInput().x;
        _input.z = _inputBridge.GetInput().y;

        return _input;
    }
}
