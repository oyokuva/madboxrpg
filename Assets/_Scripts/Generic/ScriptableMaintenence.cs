using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableMaintenence : MonoBehaviour
{
    [SerializeField]List<ScriptableObject> scriptables;

    void Start()
    {
        foreach(var scriptableObject in scriptables)    
            (scriptableObject as IResetable)?.Reset();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
