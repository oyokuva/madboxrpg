using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : ItemBase
{
    [SerializeField] float _animationSpeedMultiplier = 1f;
    [SerializeField] float _movementSpeedModifier = 1f;
    [SerializeField] float _attackRange = 2f;
    [SerializeField] private Sprite _UISprite;
    [SerializeField] private ParticleSystem _weaponFX;


    
    private BoxCollider _BodyCollider;
    private CapsuleCollider _BodyTrigger;
    private Rigidbody _Rigidbody;
    WeaponEquipManager _tempWeaponEquipManager;
    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _BodyTrigger = GetComponent<CapsuleCollider>();
        _BodyCollider = GetComponent<BoxCollider>();
       
    }

    public void VFXParticeSetEnabled(bool value)
    { 
        if(_weaponFX!=null)
            _weaponFX.gameObject.SetActive(value);
    }

    public Sprite UISprite { get { return _UISprite; } }
    public float AnimationSpeedMultiplier { get => _animationSpeedMultiplier; }
    public float MovementSpeedModifier { get => _movementSpeedModifier; }
    public float AttackRange { get => _attackRange; }

    public void SetPhysics(bool physicsEnable)
    {
        _BodyCollider.enabled = physicsEnable;
        _BodyTrigger.enabled = physicsEnable;
        _Rigidbody.isKinematic = !physicsEnable;

    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("WeaponTriggerArrived");
        _tempWeaponEquipManager = other.GetComponentInChildren<WeaponEquipManager>();
        VFXParticeSetEnabled(true);
        _tempWeaponEquipManager?.EquipWeapon(gameObject);
        
    }
}


public abstract class ItemBase : MonoBehaviour
{
    // Item Type
    // Name
}
